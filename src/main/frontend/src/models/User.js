
export class User {
    constructor(obj) {
        this.id = null;
        this.roles = [];
        for (let key in obj){
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key];
            }
        }
    }

    hasRole(role) {
        return typeof this.roles.find(el => {return el.authority.substring(5).toUpperCase() === role.toUpperCase();}) !== "undefined";
    }
}