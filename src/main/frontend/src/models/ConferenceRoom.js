export class ConferenceRoom {
    constructor(obj) {
        this.id = null;
        this.name = "";
        this.address = "";
        this.locationLink = "";
        this.capacity = "";
        this.inactive = false;

        for (let key in obj){
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key];
            }
        }
    }
}