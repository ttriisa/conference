export class Participant {
    constructor(obj) {
        this.id = null;
        this.conferenceId = "";
        this.userId = "";
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.status = null;

        for (let key in obj){
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key];
            }
        }
    }
}