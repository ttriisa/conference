export class Conference {
    constructor(obj) {
        this.id = null;
        this.name = "";
        this.conferenceRoomId = null;
        /**
         * @type {ConferenceRoom}
         */
        this.conferenceRoom = null;
        this.description = "";
        this.startsAt = null;
        this.endsAt = null;
        this.participants = 0;
        this.userParticipantLimit = -1;
        this.access = null;
        /**
         * @type {Array<Participant>}
         */
        this.participants = [];

        for (let key in obj){
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key];
            }
        }
    }
}