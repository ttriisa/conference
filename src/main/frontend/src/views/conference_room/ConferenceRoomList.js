import React, {useEffect, useState} from "react"
import {authenticationService} from "../../services/authentication.service"
import {conferenceRoomService} from "../../services/conferenceRoom.service"
import {Link, navigate} from "@reach/router/index"
import update from "immutability-helper"

export function ConferenceRoomList() {
    const [rooms, setRooms] = useState([]);
    const [currentUser, setCurrentUser] = useState(authenticationService.currentUserValue);

    useEffect(() => {
        conferenceRoomService.getAll().then(setRooms);

        const authSubscription = authenticationService.currentUser.subscribe(setCurrentUser);
        return () => { authSubscription.unsubscribe(); };
    }, []);

    function removeItem(item) {
        conferenceRoomService.remove(item.id).then(() => {
            setRooms(update(rooms, {$splice: [[rooms.indexOf(item), 1]]}));
        });
    }

    return (
        <div className="conference-list">
            <h2>Rooms</h2>
            {currentUser && <div className="actions">
                <button className="btn btn-primary" onClick={navigate.bind(null, "/rooms/create")}>Create new room</button>
            </div>}
            {rooms &&
            <table className="table">
                <thead>
                <tr><th>Name</th><th>Capacity</th><th>Actions</th></tr>
                </thead>
                <tbody>
                {rooms.map(room =>
                    <tr key={room.id}>
                        <td>
                            <Link to={"/rooms/"+room.id}>{room.name}</Link><br/>
                            {room.locationLink.length > 0 ?
                                <small><a className="text-muted" target="_blank" rel="noopener noreferrer" href={room.locationLink}>{room.address}</a></small>
                                :
                                <small className="text-muted">{room.address}</small>
                            }
                        </td>
                        <td>{room.capacity}</td>
                        <td>
                            {currentUser && <div className="actions">
                                <button onClick={navigate.bind(null, `/rooms/${room.id}/edit`)} className="btn btn-success">Edit</button>
                                <button onClick={removeItem.bind(null, room)} className="btn btn-danger">Delete</button>
                            </div>}
                        </td>
                    </tr>
                )}
                </tbody>
            </table>}
            {rooms.length === 0 && <em>Empty list</em>}
        </div>
    );
}