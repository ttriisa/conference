import React, {useEffect, useState} from "react"
import {authenticationService} from "../../services/authentication.service"
import {conferenceRoomService} from "../../services/conferenceRoom.service"
import {navigate} from "@reach/router/index"
import {ConferenceRoom} from "../../models/ConferenceRoom"
import update from "immutability-helper"

export function ConferenceRoomForm(props) {
    const [conferenceRoom, setConference] = useState(new ConferenceRoom());
    const [currentUser, setCurrentUser] = useState(authenticationService.currentUserValue);

    const [errors, setErrors] = useState([]);
    const [isWaitingResponse, setIsWaitingResponse] = useState(false);
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const authSubscription = authenticationService.currentUser.subscribe(setCurrentUser);
        return () => { authSubscription.unsubscribe(); };
    }, []);

    useEffect(() => {
        if(props.id) conferenceRoomService.get(props.id).then(setConference);
    }, [props.id]);

    function handleChange(e) {
        const { name, value } = e.target;
        setConference(update(conferenceRoom, {[name]: {$set: value}}));
    }

    function handleSubmit(e) {
        e.preventDefault();
        setSubmitted(true);

        conferenceRoom.capacity = parseInt(conferenceRoom.capacity);
        if(isNaN(conferenceRoom.capacity)) {
            conferenceRoom.capacity = null;
        } else if(conferenceRoom.capacity < 1) {
            return;
        }

        // quick validation
        if (!(conferenceRoom.name)) {
            return;
        }

        setIsWaitingResponse(true);
        conferenceRoomService.save(conferenceRoom).then(
            navigate.bind(null, "/rooms"),
            (err) => {
                setErrors(err);
                setIsWaitingResponse(false);
            }
        );
    }

    return (
        <div className="conference-room-form">
            <h2>{conferenceRoom.id == null ? "Create new conference room" : `Edit conference room: ${conferenceRoom.name}`}</h2>

            {errors.length > 0 &&
            <div className='alert alert-danger'>{errors}</div>
            }
            <form name="form" onSubmit={handleSubmit}>
                <div className={'form-group' + (submitted && !conferenceRoom.name ? ' has-error' : '')}>
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" name="name" value={conferenceRoom.name} onChange={handleChange} />
                    {submitted && !conferenceRoom.name &&
                    <div className="help-block">Name is required</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !conferenceRoom.address ? ' has-error' : '')}>
                    <label htmlFor="name">Address</label>
                    <input type="text" className="form-control" name="address" value={conferenceRoom.address} onChange={handleChange} />
                    {submitted && !conferenceRoom.address &&
                    <div className="help-block">Address is required (in range of 2-255 characters)</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !conferenceRoom.locationLink ? ' has-error' : '')}>
                    <label htmlFor="name">Location link (optional)</label>
                    <input type="text" className="form-control" name="locationLink" value={conferenceRoom.locationLink} onChange={handleChange} />
                    {conferenceRoom.locationLink.length > 255 &&
                    <div className="help-block">Location link can't be longer than 255 characters</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !conferenceRoom.capacity ? ' has-error' : '')}>
                    <label htmlFor="name">Capacity (optional)</label>
                    <input type="type" className="form-control" name="capacity" value={conferenceRoom.capacity} onChange={handleChange} />
                    <div className="help-block">{conferenceRoom.capacity < 1 && conferenceRoom.capacity !== "" ?
                        "Capacity must be a positive integer or empty when the capacity is unknown." :
                        "Leave the field empty when the capacity is unknown or unlimited."}</div>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" disabled={isWaitingResponse}>{conferenceRoom.id == null ? "Create" : "Save"}</button>
                </div>
            </form>
        </div>
    );
}