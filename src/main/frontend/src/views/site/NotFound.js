import React from "react"

export function NotFound() {
    return <div className="not-found"><h2>Page not found</h2><p>¯\_(ツ)_/¯</p></div>;
}