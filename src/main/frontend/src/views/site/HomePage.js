import React from 'react';

export function HomePage() {
    return (
        <div className="home-page">
            <h2>Home</h2>
            <p>Hello, Potion Seller, I am going into battle and I want your strongest potions.</p>
        </div>
    );
}