import React, { useState } from 'react';
import {authenticationService} from "../../services"
import {navigate} from "@reach/router/index"

export function LoginPage() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState([]);
    const [isWaitingResponse, setIsWaitingResponse] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const fieldSetterMap = {
        username: setUsername,
        password: setPassword
    }

    function handleChange(e) {
        const { name, value } = e.target;
        fieldSetterMap[name](value);
    }

    function handleSubmit(e) {
        e.preventDefault();
        setSubmitted(true);

        // quick validation
        if (!(username && password)) {
            return;
        }

        setIsWaitingResponse(true);
        authenticationService.login(username, password)
            .then(
                user => {
                    setIsWaitingResponse(false);
                    navigate("/conferences");
                },
                error => {
                    if(error instanceof Error)
                        setErrors(error.message);
                    else
                        setErrors(error);
                    setIsWaitingResponse(false);
                }
            );
    }

    return (
        <div className="login-page">
            <h2>Login</h2>
            {errors.length > 0 &&
            <div className='alert alert-danger'>{errors}</div>
            }
            <form name="form" onSubmit={handleSubmit}>
                <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                    <label htmlFor="username">Username</label>
                    <input type="text" className="form-control" name="username" value={username} onChange={handleChange} />
                    {submitted && !username &&
                    <div className="help-block">Username is required</div>
                    }
                </div>
                <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" name="password" value={password} onChange={handleChange} />
                    {submitted && !password &&
                    <div className="help-block">Password is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" disabled={isWaitingResponse}>Login</button>
                </div>
            </form>
        </div>
    );
}