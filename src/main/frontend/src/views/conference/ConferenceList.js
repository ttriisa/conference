import React, {useEffect, useState} from "react"
import {authenticationService} from "../../services/authentication.service"
import {conferenceService} from "../../services/conference.service"
import {Link, navigate} from "@reach/router/index"
import update from "immutability-helper"

export function ConferenceList() {
    const [conferences, setConferences] = useState([]);
    const [currentUser, setCurrentUser] = useState(authenticationService.currentUserValue);

    useEffect(() => {
        conferenceService.getAll().then(setConferences);

        const authSubscription = authenticationService.currentUser.subscribe(setCurrentUser);
        return () => { authSubscription.unsubscribe(); };
    }, []);

    /**
     * Returns participant count with a limit when applicable
     * @returns {String}
     */
    function participantStatsShort(c) {
        if(typeof c.participants === "undefined") return "0";
        if(typeof c.userParticipantLimit !== "undefined") {
            if (c.userParticipantLimit > 0)
                return `${c.participants}/${c.userParticipantLimit}`;
        }
        return c.participants.toString();
    }

    function removeItem(item) {
        conferenceService.remove(item.id).then(() => {
            setConferences(update(conferences, {$splice: [[conferences.indexOf(item), 1]]}));
        });
    }

    return (
        <div className="conference-list">
            <h2>Conferences</h2>
            {currentUser && <div className="actions">
                <button className="btn btn-primary" onClick={navigate.bind(null, "/conferences/create")}>Create new conference</button>
            </div>}
            {conferences &&
            <table className="table">
                <thead>
                    <tr><th style={{width: "60%"}}>Name</th><th style={{width: "15%"}}>Participants</th><th style={{width: "25%"}}>Actions</th></tr>
                </thead>
                <tbody>
                {conferences.map(conference =>
                    <tr key={conference.id}>
                        <td><Link to={"/conferences/"+conference.id}>{conference.name}</Link></td>
                        <td>{participantStatsShort(conference)}</td>
                        <td>
                            {currentUser && <div className="actions">
                                <button onClick={navigate.bind(null, `/conferences/${conference.id}/edit`)} className="btn btn-success">Edit</button>
                                <button onClick={removeItem.bind(null, conference)} className="btn btn-danger">Delete</button>
                            </div>}
                        </td>
                    </tr>
                )}
                </tbody>
            </table>}
            {conferences.length === 0 && <em>Empty list</em>}
        </div>
    );
}