import React, {useEffect, useState} from "react"
import {authenticationService} from "../../services/authentication.service"
import {conferenceService} from "../../services/conference.service"
import {navigate} from "@reach/router/index"
import {Conference} from "../../models/Conference"
import update from "immutability-helper"
import {conferenceRoomService} from "../../services/conferenceRoom.service"

export function ConferenceForm(props) {
    const [conference, setConference] = useState(new Conference());
    const [currentUser, setCurrentUser] = useState(authenticationService.currentUserValue);

    const [errors, setErrors] = useState([]);
    const [isWaitingResponse, setIsWaitingResponse] = useState(false);
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const authSubscription = authenticationService.currentUser.subscribe(setCurrentUser);
        return () => { authSubscription.unsubscribe(); };
    }, []);

    useEffect(() => {
        if(props.id) conferenceService.get(props.id).then(setConference);
    }, [props.id]);

    function handleChange(e) {
        const { name, value } = e.target;
        setConference(update(conference, {[name]: {$set: value}}));
    }

    function handleSubmit(e) {
        e.preventDefault();
        setSubmitted(true);

        // quick validation
        if (!(conference.name)) {
            return;
        }

        setIsWaitingResponse(true);
        conferenceService.save(conference).then(
            navigate.bind(null, "/conferences"),
            (err) => {
                setErrors(err);
                setIsWaitingResponse(false);
            }
        );
    }

    return (
        <div className="conference-form">
            <h2>{conference.id == null ? "Create new conference" : `Edit conference: ${conference.name}`}</h2>

            {errors.length > 0 &&
            <div className='alert alert-danger'>{errors}</div>
            }
            <form name="form" onSubmit={handleSubmit}>
                <div className={'form-group' + (submitted && !conference.name ? ' has-error' : '')}>
                    <label htmlFor="name">Name</label>
                    <input type="text" className="form-control" name="name" value={conference.name} onChange={handleChange} />
                    {submitted && !conference.name &&
                    <div className="help-block">Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" disabled={isWaitingResponse}>{conference.id == null ? "Create" : "Save"}</button>
                </div>
            </form>
        </div>
    );
}