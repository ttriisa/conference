import config from '../config';
import { authHeader, handleApiResponse } from '../helpers';
import {RestService} from "./rest.service"

export class ConferenceService extends RestService {
    constructor() {
        super("conferences");
    }
}

export const conferenceService = new ConferenceService();

/*
export const conferenceService = {
    getAll,
    get,
    save,
    remove
};

function getAll() {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(config.apiEndpoint+"/conferences", requestOptions).then(handleApiResponse).then(x => {return x._embedded.conferences});
}

function get(id) {
    const requestOptions = { method: 'GET', headers: authHeader() };
    return fetch(config.apiEndpoint+"/conferences/"+id, requestOptions).then(handleApiResponse).then(x => {console.log(x); return x._embedded.conference});
}

function save(model) {
    let method = "POST", url = "/conferences";
    if(model.id != null) {
        method = "PUT";
        url = "/conferences/"+model.id;
    }

    const requestOptions = { method: method, headers: authHeader(), body: JSON.stringify(model) };
    return fetch(config.apiEndpoint+url, requestOptions).then(handleApiResponse).then(x => {console.log(x); return x._embedded.conference});
}

function remove(id) {
    const requestOptions = { method: 'DELETE', headers: authHeader() };
    return fetch(config.apiEndpoint+"/conferences/"+id, requestOptions).then(handleApiResponse);
}*/