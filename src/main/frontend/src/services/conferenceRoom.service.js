import {RestService} from "./rest.service"

export class ConferenceRoomService extends RestService {
    constructor() {
        super("conferenceRooms");
    }
}

export const conferenceRoomService = new ConferenceRoomService();