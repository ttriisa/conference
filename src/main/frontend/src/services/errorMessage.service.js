import { BehaviorSubject } from 'rxjs';

import update from "immutability-helper/index"

const globalErrorsSubject = new BehaviorSubject([]);

export const errorMessageService = {
    addError,
    setError,
    clearErrors,
    globalErrors: globalErrorsSubject.asObservable(),
    get globalErrorsValue () { return globalErrorsSubject.value }
};

function addError(message) {
    globalErrorsSubject.next(update(globalErrorsSubject.value, {$push: [message]}));
}

function setError(message) {
    globalErrorsSubject.next([message]);
}

function clearErrors() {
    if(globalErrorsSubject.value.length)
        globalErrorsSubject.next([]);
}