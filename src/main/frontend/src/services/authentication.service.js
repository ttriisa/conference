import { BehaviorSubject } from 'rxjs';

import config from '../config';
import {User} from "../models/User"

// Try to create User instance by saved data, otherwise keep currentUser null
let currentUserData = localStorage.getItem('currentUser');
if(currentUserData != null)
    currentUserData = new User(JSON.parse(currentUserData));
const currentUserSubject = new BehaviorSubject(currentUserData);

export const authenticationService = {
    login,
    logout,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(config.apiEndpoint+"/authenticate", requestOptions)
        .then(response => {
            if (response.ok)
                response.text().then(JSON.parse).then(
                    response => {
                        const user = new User(response.user);
                        user.token = response.tokenValue;
                        // Store user details and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('currentUser', JSON.stringify(user));
                        //console.log(user, JSON.stringify(user));
                        // BUG for some reason following line will throw an error occasionally
                        currentUserSubject.next(user);
                        return user;
                });
            else
                return Promise.reject("Invalid Login Credentials");
        })
}

function logout() {
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}