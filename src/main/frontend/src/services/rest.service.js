import config from '../config';
import {authHeader, handleApiResponse, handleNetworkError} from '../helpers';

export class RestService {
    constructor(modelName) {
        this.modelName = modelName;
    }

    getAll() {
        const requestOptions = { method: 'GET', headers: authHeader() };
        return fetch(config.apiEndpoint+"/"+this.modelName, requestOptions).then(handleApiResponse, handleNetworkError).then(x => {return x._embedded[this.modelName]});
    }

    get(id) {
        const requestOptions = { method: 'GET', headers: authHeader() };
        return fetch(config.apiEndpoint+"/"+this.modelName+"/"+id, requestOptions).then(handleApiResponse, handleNetworkError);
    }

    save(model) {
        let method = "POST", url = "/"+this.modelName;
        if(model.id != null) {
            method = "PUT";
            url = "/"+this.modelName+"/"+model.id;
        }

        const requestOptions = { method: method, headers: {...authHeader(), 'Content-Type': 'application/json'}, body: JSON.stringify(model) };
        return fetch(config.apiEndpoint+url, requestOptions).then(handleApiResponse, handleNetworkError);
    }

    remove(id) {
        const requestOptions = { method: 'DELETE', headers: authHeader() };
        return fetch(config.apiEndpoint+"/"+this.modelName+"/"+id, requestOptions).then(handleApiResponse, handleNetworkError);
    }
};