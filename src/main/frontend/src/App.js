import React, { useState, useEffect } from 'react';
import './App.css';
import {Link, Location, navigate, Redirect, Router} from "@reach/router/index"
import {NotFound, HomePage, LoginPage, ConferenceList, ConferenceRoomList, ConferenceRoomForm} from "./views"
import {authenticationService} from "./services/authentication.service"
import {ConferenceForm} from "./views/conference/ConferenceForm"
import {errorMessageService} from "./services/errorMessage.service"

export default function App() {
    const [currentUser, setCurrentUser] = useState(null);
    const [globalErrors, setGlobalErrors] = useState([]);

    const [currentLocation, setCurrentLocation] = useState("/");

    useEffect(() => {
        const authSub = authenticationService.currentUser.subscribe(setCurrentUser);
        const errorsSub = errorMessageService.globalErrors.subscribe(setGlobalErrors);

        return () => {
            // Q: Does it even run?
            authSub.unsubscribe();
            errorsSub.unsubscribe();
        };
    }, []);

    useEffect(() => {
        errorMessageService.clearErrors();
    }, [currentLocation]);

    return (
        <div>
            <header className="app-header">
                <h1>ConferenceApp</h1>
                <nav id="main-nav navbar navbar-light bg-light" role="navigation" aria-label="Main">
                    <ul className="nav" role="menubar">
                        <li className="nav-item"><Link to="/" className="nav-link" role="menuitem">Home</Link></li>
                        <li className="nav-item"><Link to="/conferences" className="nav-link" role="menuitem">Conferences</Link></li>
                        {currentUser &&
                            <>
                                <li className="nav-item"><Link to="/rooms" className="nav-link" role="menuitem">Rooms</Link></li>
                                {currentUser.hasRole('ADMIN') && <li><Link to="/users" className="nav-link" role="menuitem">Users</Link></li>}
                                <li className="nav-item"><a href="/logout" className="nav-link" onClick={(e) => {
                                    e.preventDefault();
                                    authenticationService.logout();
                                    navigate("/");
                                }} role="menuitem">Logout</a></li>
                            </>}
                        {!currentUser && <li className="nav-item"><Link to="/login" className="nav-link" role="menuitem">Login</Link></li>}
                    </ul>
                </nav>
            </header>
            <hr />
            {globalErrors.length > 0 && <div className="global-errors alert alert-danger" role="alert">{globalErrors}</div>}

            <Router>
                {currentUser && <Redirect from="/login" to="/" />}
                <HomePage path="/" />
                <LoginPage path="/login" />
                <ConferenceList path="/conferences" />
                <ConferenceForm path="/conferences/create" />
                <ConferenceForm path="/conferences/:id" />
                <ConferenceForm path="/conferences/:id/edit" />
                <ConferenceRoomList path="rooms" />
                <ConferenceRoomForm path="/rooms/create" />
                <ConferenceRoomForm path="/rooms/:id" />
                <ConferenceRoomForm path="/rooms/:id/edit" />
                {/* <UserList path="users" /> */}
                <NotFound default />
            </Router>

            <Location>
                {({ location })=> {
                    setCurrentLocation(location.pathname);
                }}
            </Location>
        </div>
    );
}