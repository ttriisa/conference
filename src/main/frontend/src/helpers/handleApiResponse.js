import {navigate} from "@reach/router/index"

export default function handleApiResponse(response) {
    return response.text().then(text => {
        let data = text && JSON.parse(text);
        if (!response.ok) {
            switch(response.status) {
                case 401: navigate("/login"); break;
                case 403: //todo render no permission view or smth; break;
                case 404: data = {message: "Network Error: page not found"}; break;
                default: break;
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}