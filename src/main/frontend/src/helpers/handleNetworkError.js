import {errorMessageService} from "../services/errorMessage.service"

export default function handleNetworkError(error) {
    errorMessageService.setError(error && error.message);
    return Promise.reject(error);
}