DROP SCHEMA public CASCADE;

CREATE TABLE user_role (
   id BIGINT NOT NULL PRIMARY KEY,
   user_id BIGINT NOT NULL,
   name VARCHAR(255) NOT NULL
);

CREATE TABLE user (
   id BIGINT NOT NULL PRIMARY KEY,
   username VARCHAR(255) NOT NULL,
   password VARCHAR(255) NOT NULL,
   first_name VARCHAR(255) NOT NULL,
   last_name VARCHAR(255) NOT NULL,
   email VARCHAR(255) NOT NULL
);

CREATE TABLE conference_room (
   id BIGINT NOT NULL PRIMARY KEY,
   name VARCHAR(255) NOT NULL,
   address VARCHAR(255) NULL,
   location_link VARCHAR(255) NULL,
   capacity INT NULL,
   inactive BOOLEAN NOT NULL
);

CREATE TABLE conference (
   id BIGINT NOT NULL PRIMARY KEY,
   owner_id BIGINT NOT NULL,
   conference_room_id BIGINT NULL DEFAULT NULL,
   name VARCHAR(255) NOT NULL,
   description CLOB NULL,
   starts_at TIMESTAMP NULL,
   ends_at TIMESTAMP NULL,
   user_participant_limit INT NOT NULL,
   access TINYINT NOT NULL
);

CREATE TABLE participant (
   id BIGINT NOT NULL PRIMARY KEY,
   conference_id BIGINT NOT NULL,
   user_id BIGINT NULL,
   first_name VARCHAR(255) NULL,
   last_name VARCHAR(255) NULL,
   email VARCHAR(255) NULL,
   status TINYINT NOT NULL,
   access_token VARCHAR(1024) NOT NULL
);

INSERT INTO user (id, username, password) VALUES (1,'admin','$2a$10$ZtnT49eKu89aFBbCYaU4QuC1j3ldkr2IggWUPGyHfmFgok7cMGJ5G');
INSERT INTO user (id, username, password) VALUES (2,'user','$2a$10$/5drxb3T146h3h2X/Da3te1HtJ/E7f6aOPuMehSfd8roMSvIPtuz6');

INSERT INTO user_role(id, user_id, name) VALUES (1, 1, 'ROLE_ADMIN');
INSERT INTO user_role(id, user_id, name) VALUES (2, 2, 'ROLE_USER');