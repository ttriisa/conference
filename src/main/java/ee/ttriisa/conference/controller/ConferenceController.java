package ee.ttriisa.conference.controller;

import ee.ttriisa.conference.model.Conference;
import ee.ttriisa.conference.repository.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConferenceController {
    private static final String template = "Hello, %s!";

    @Autowired
    private ConferenceRepository repository;

    @GetMapping("/list")
    Iterable<Conference> all() {
        return repository.findAll();
    }

    @RequestMapping("/conference")
    public String all(@RequestParam(value="name", defaultValue="World") String name) {
        return String.format(template, name);
    }
}
