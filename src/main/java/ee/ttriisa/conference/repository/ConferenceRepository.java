package ee.ttriisa.conference.repository;

import ee.ttriisa.conference.model.Conference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConferenceRepository extends CrudRepository<Conference, Long> {
    //public Integer getParticipantCount()

    /*@PreAuthorize(value = "hasAuthority('ADMIN')"
        + "or authentication.principal.equals(#post.member) ")*/
}
