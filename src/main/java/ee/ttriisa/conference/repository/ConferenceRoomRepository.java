package ee.ttriisa.conference.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ee.ttriisa.conference.model.ConferenceRoom;

@Repository
public interface ConferenceRoomRepository extends CrudRepository<ConferenceRoom, Long> {}
