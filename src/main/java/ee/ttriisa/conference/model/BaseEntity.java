package ee.ttriisa.conference.model;

import javax.persistence.EntityListeners;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

//@Entity
//@Table(name = "", schema = "", catalog = "")
@EntityListeners(BaseEntity.class)
public class BaseEntity {
    @PrePersist
    public void beforePersist(final BaseEntity reference) {}

    @PreUpdate
    public void beforeUpdate(final BaseEntity reference) {}
}