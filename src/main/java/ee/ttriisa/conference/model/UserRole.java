package ee.ttriisa.conference.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_role")
public class UserRole implements GrantedAuthority {
    public enum Role {
        ROLE_ADMIN,
        ROLE_USER
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private Role name;

    @Override
    public String getAuthority() {
        return this.name.name();
    }

    public UserRole() {}

    public UserRole(Role name) {
        this.name = name;
    }
}
