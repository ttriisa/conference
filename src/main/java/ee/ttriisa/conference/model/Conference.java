package ee.ttriisa.conference.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Table(name="conference")
public class Conference {
    public enum Access {
        INVITE_ONLY,
        PUBLIC
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 64)
    private String name;

    @Size(max = 16384)
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "starts_at")
    private Date startsAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ends_at")
    private Date endsAt;

    /**
     * This is a limit set by the owner/host, it can exceed the actual room capacity.
     * Users without invite can't join this event when the userParticipantLimit is reached.
     * For example host may know that the venue has extra space for standing or somebody is not coming.
     * Participants with AttendingStatus::DECLINED are not counted towards userParticipantLimit.
     * By design userParticipantLimit should default to the room capacity in UI.
     */
    @Column(name = "user_participant_limit")
    private Integer userParticipantLimit;

    private Access access;

    /*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="conference_id", nullable = false)
    private List<Participant> participants;
*/
    @ManyToOne
    @JoinColumn
    @CreatedBy
    private User owner;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn
    private ConferenceRoom room;


    public Conference() {}

    public Conference(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
