package ee.ttriisa.conference.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Entity
@Table(name="conference_room")
public class ConferenceRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 2, max = 255)
    private String name;

    @Size(min = 2, max = 255)
    private String address;

    private Integer capacity;

    @Column(name = "location_link")
    private String locationLink;

    private Boolean inactive = false;

    /*@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinColumn(name="conference_room_id", nullable = false)
    private List<Conference> conferences;*/

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="conference_room_id")
    private Set<Conference> conferences;

    public ConferenceRoom() {}

    public ConferenceRoom(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
