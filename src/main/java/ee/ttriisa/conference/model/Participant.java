package ee.ttriisa.conference.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Data
@Entity
@Table(name="participant")
public class Participant extends BaseEntity {
    public enum AttendingStatus {
        GOING,
        MAYBE,
        DECLINED,
        INVITED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Size(min = 2, max = 255)
    private String first_name;

    @Size(min = 2, max = 255)
    private String last_name;

    @Size(min = 3, max = 255)
    private String email;

    @NotNull
    private AttendingStatus status;

    @NotNull
    @Column(name = "access_token")
    @JsonIgnore
    private String accessToken;

    private void generateAccessToken() {
        this.accessToken = new Base64StringKeyGenerator(128).generateKey();
    }

    @Override
    public void beforePersist(final BaseEntity reference) {
        this.generateAccessToken();
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", Objects.toString(this.first_name, ""), Objects.toString(this.last_name, ""), Objects.toString(this.email, ""));
    }
}
