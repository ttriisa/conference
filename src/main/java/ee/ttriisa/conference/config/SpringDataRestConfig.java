package ee.ttriisa.conference.config;

import ee.ttriisa.conference.model.Conference;
import ee.ttriisa.conference.model.ConferenceRoom;
import ee.ttriisa.conference.model.Participant;
import ee.ttriisa.conference.model.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
class SpringDataRestConfig {

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurer() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.exposeIdsFor(Conference.class, ConferenceRoom.class, User.class, Participant.class);
            }
        };
    }
}